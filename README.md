# Message Form
This is a simple message form that emails the contents to you via Sparkpost.
Things needed:
 1. ReCapcha API Keys
 2. Sparkpost Account (500 Free Emails / Month)
 3. A domain name to send emails from.

Files to change.
 1. index.html (Line 80) ReCaptcha Client Site Key
 2. /php/contact.php (Lines 3 - 15) Per site settings
 3. (Optional but recommended) Success and Failure actions in contact.php (Lines 39 & 58)