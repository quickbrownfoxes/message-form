<?php

//Per site settings
$recaptchasecret = ""; //Google Recaptcha secret
// See https://www.google.com/recaptcha/admin

$sparkpost_apikey = "Authorization: 1234"; //Only change from 1234 onward 'Authorization: ' must stay.
// Must have "Transmissions: Read/Write" Permissions - https://app.sparkpost.com/account/api-keys

$send_from_email = "example@gmail.com"; //Must be a verifed domain 
// See https://app.sparkpost.com/account/sending-domains

$send_to_email = "example2@gmail.com";
$subject_of_email = "Contact Form";
//End Settings

$host  = $_SERVER['HTTP_REFERER'];
$success = "#success";
$failcaptcha = "#failcaptcha";
$failempty = "#failempty";
$failemail = "#failemail";

if(!empty($_POST["name"]) && !empty($_POST["email"]) && !empty($_POST["message"]) && !empty($_POST["g-recaptcha-response"])) {
	$name = $_POST["name"];
	$email = $_POST["email"];
	$message = $_POST["message"];
	$response = $_POST["g-recaptcha-response"];
} else {
	header("Location: $host$failempty"); 
	return false;
}

if (!empty($response)) {
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$data = array(
		'secret' => $recaptchasecret,
		'response' => $_POST["g-recaptcha-response"]
	);
	$options = array(
		'http' => array(
			'header' => 'Content-Type: application/x-www-form-urlencoded',
			'method' => 'POST',
			'content' => http_build_query($data)
		)
	);
	$context = stream_context_create($options);
	$verify = file_get_contents($url, false, $context);
	$captcha_success = json_decode($verify);
	if ($captcha_success->success == false) {
		header("Location: https://$host$failcaptcha"); //What to do when the captcha fails
	} else if ($captcha_success->success == true) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.sparkpost.com/api/v1/transmissions');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n      \"options\": { \"sandbox\": false },\n      \"content\": {\n      \"from\": \"$send_from_email\",\n      \"subject\": \"$subject_of_email\",\n      \"html\": \"<html><body><p>Name: $name</p><p>Email Address: $email</p><p>Message: $message</p></body></html>\"\n    },\n    \"recipients\": [ { \"address\": \"$send_to_email\" } ]\n  }");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = $sparkpost_apikey;
		$headers[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			header("Location: $host$failemail"); //What do do when the email sending fails
		} else {
			header("Location: $host$success"); //What do do when the email is sent successfully
		}
		curl_close($ch);
	}
} else {
	header("Location: $host$failcaptcha"); 
}

?>